package at.ac.tuwien.par.amp;

import at.ac.tuwien.blt.benchmark.BenchmarkRunner;
import at.ac.tuwien.blt.benchmark.builder.scenario.BenchmarkScenarioBuilder;
import at.ac.tuwien.blt.benchmark.builder.scenario.ThreadScenarioBuilder;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkConfiguration;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptions;
import at.ac.tuwien.blt.benchmark.config.options.BenchmarkOptionsBuilder;
import at.ac.tuwien.blt.benchmark.eval.ResultConfiguration;
import at.ac.tuwien.blt.lincheck.LinCheckRunner;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BLT {
    public static void main(String[] args) throws NoSuchMethodException {
        argParseExample(args);
    }

    private static void argParseExample(String[] args) throws NoSuchMethodException {
        ArgumentParser parser = ArgumentParsers.newFor("AMP_BLT").build()
                .defaultHelp(true)
                .description("Benchmark and linearizability-checking framework for concurrent data structures");
        parser.addArgument("-b", "--benchmark")
                .type(Integer.class)
                .metavar("BENCHMARK_NUM")
                .setDefault(0)
                .help("Which benchmark to use");
        parser.addArgument("-l", "--lincheck")
                .action(Arguments.storeTrue())
                .setDefault(false)
                .help("Whether to run lincheck in addition to benchmarks (default is false)");
        parser.addArgument("-lo", "--lincheck-only")
                .action(Arguments.storeTrue())
                .setDefault(false)
                .help("If set, run no benchmarks, only lincheck");
        try {
            Namespace ns = parser.parseArgs(args);
            int benchmarkNum = ns.get("benchmark");
            boolean runLincheck = ns.get("lincheck");
            boolean lincheckOnly = ns.get("lincheck_only");
            runBenchmark(benchmarkNum, runLincheck, lincheckOnly);
            System.exit(0);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
    }

    @SuppressWarnings("SwitchStatementWithTooFewBranches")
    private static void runBenchmark(int benchmarkNum, boolean runLincheck, boolean lincheckOnly) throws NoSuchMethodException {
        switch (benchmarkNum) {
            case 1:
                String resultFolder;
                if (lincheckOnly) {
                    resultFolder = createResultDirectories(new BenchmarkOptionsBuilder().build());
                    if (runLincheck) {
                        queueLincheck(resultFolder);
                    }
                } else {
                    resultFolder = queueBenchmark();
                    if (runLincheck) {
                        queueLincheck(resultFolder);
                    }
                }
                break;
            default:
                System.err.printf("No benchmark could be found for the provided benchmark number \"%d\", exiting.\n",
                        benchmarkNum);
                System.exit(1);
        }
    }


    private static String queueBenchmark() throws NoSuchMethodException {
        var wrapperClass = MyConcurrentQueueWrapper.class;
        var con = wrapperClass.getMethod("contains", Integer.class);
        var add = wrapperClass.getMethod("add", Integer.class);
        var ts = new ThreadScenarioBuilder(wrapperClass)
                .addMethodCall(con).build();
        var benchmarkScenario = new BenchmarkScenarioBuilder()
                .scenarioSize(2).addThreadScenario(ts).build();
        var initScenario = new ThreadScenarioBuilder(wrapperClass)
                .addMultipleMethodCalls(10, add).build();

        BenchmarkOptions opt = new BenchmarkOptionsBuilder()
                .benchmarkScenario(benchmarkScenario)
                .initScenario(initScenario)
                .measurementIterations(3)
                .threads(1,2,3,4)
                .build();
        BenchmarkRunner.run(MyConcurrentQueueWrapper.class, opt);
        return opt.getFolderName();
    }

    private static void queueLincheck(String resultFolder) {
        LinCheckRunner.run(MyConcurrentQueueWrapper.class, resultFolder);
    }

    private static String createResultDirectories(BenchmarkOptions benchmarkOptions) {
        var resultName = benchmarkOptions.getResultFolder();
        var folderName = benchmarkOptions.getFolderName();
        Path resultPath = Paths.get(resultName);
        if (Files.exists(resultPath)) {
            try {
                Files.createDirectories(resultPath);
            } catch (IOException var7) {
                throw new RuntimeException("Could not create result data directory in \"" + resultPath + "\"");
            }
        }

        Path folderPath = Paths.get(folderName);

        try {
            Files.createDirectories(folderPath);
        } catch (IOException var6) {
            throw new RuntimeException("Could not create result data directory in \"" + folderPath + "\"");
        }
        return folderName;
    }

}
