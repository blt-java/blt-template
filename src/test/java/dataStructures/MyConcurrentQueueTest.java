package dataStructures;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyConcurrentQueueTest {
    @Test
    void queueTest() {
        var q = new MyConcurrentQueue<>();
        q.add(1);
        q.add(2);
        q.add(3);
        q.remove(2);
        assertTrue(q.contains(1));
        assertTrue(q.contains(3));
        assertFalse(q.contains(2));
    }
}