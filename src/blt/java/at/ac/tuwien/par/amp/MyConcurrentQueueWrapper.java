package at.ac.tuwien.par.amp;

import at.ac.tuwien.blt.benchmark.annotations.BenchmarkWrapper;
import dataStructures.MyConcurrentQueue;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.kotlinx.lincheck.annotations.Operation;
import org.jetbrains.kotlinx.lincheck.annotations.Param;
import org.jetbrains.kotlinx.lincheck.annotations.StateRepresentation;
import org.jetbrains.kotlinx.lincheck.paramgen.IntGen;
import org.jetbrains.kotlinx.lincheck.strategy.stress.StressCTest;
import org.jetbrains.kotlinx.lincheck.verifier.VerifierState;

import java.util.concurrent.ConcurrentLinkedQueue;

@BenchmarkWrapper
@Param(name = "val", gen = IntGen.class, conf = "1:5")
public class MyConcurrentQueueWrapper extends VerifierState {
    private final MyConcurrentQueue<Integer> q = new MyConcurrentQueue<>();

    @Operation
    public boolean add(Integer val) {
        return q.add(val);
    }

    @Operation
    public boolean remove(Integer val) {
        return q.remove(val);
    }

    @Operation
    public boolean contains(Integer val) {
        return q.contains(val);
    }

    @Override
    @NotNull
    protected Object extractState() {
        return q.toString();
    }
}
